var last_formdata = "";
var _field;
var _fieldname;
var _callback;
var _action;
var st = -1;
var timeout = 500;

Drupal.behaviors.ajax_trigger = function() {

    for (var form_id in Drupal.settings.ajax_trigger) {

        var form_settings = Drupal.settings.ajax_trigger[form_id];
        var jsonstr = form_settings.jsonstr;
        var timeout = form_settings.timeout;

        eval('var data = ' + jsonstr);

        var callback = function(index, item) {
            
            var field = $("select[name^='"+index+"'],textarea[name^='"+index+"'],input[name^='"+index+"'],*[id^='"+index+"']");

            var callback = item["ajax_callback"];
            var callback2 = function(event) {
                eval('$(field).'+event+'(function() { ajax_trigger_prepost(field,index,callback,event); });');
            };

            for ( name in item["javascript_event"] ) {
              if ( callback2.call( item["javascript_event"][ name ], name, item["javascript_event"][ name ] ) === false ) break; 
            };
            
         };

         for ( name in data ) {
             if ( callback.call( data[ name ], name, data[ name ] ) === false ) break; 
         };

         $("#ajax_trigger").val("").remove();
         $("#ajax_trigger").val("").remove();
         $("#ajax_trigger_timeout").val("").remove();
         $("#ajax_trigger_timeout").val("").remove();
    };
};

function ajax_trigger_result(data) {
    eval('var json_data = ' + data); 
    $.each(json_data, function(a,b) {
        eval(b);
    });
};


function ajax_trigger_post(field, fieldname, callback, action) {
    var formdata = $(field).parents("form:first").serialize();
    if (last_formdata != formdata) {
       $.post("/ajax_trigger/" + fieldname,{"data": formdata, "action": action, "callback": callback},ajax_trigger_result,"text");
       last_formdata = formdata;
    };
};


function ajax_trigger_prepost(field, fieldname, callback, action) {
    _field = field;
    _fieldname = fieldname;
    _callback = callback;
    _action = action;
    if (st > 0) clearTimeout(st);
    
    st = setTimeout('ajax_trigger_post(_field, _fieldname, _callback, _action)', timeout);
}

