<?php

/**
 * @file
 *   Form settings include file
 *
 * @version
 *
 * @developers
 *   Rafal Wieczorek <kenorb@gmail.com>
 */


/**
 *  Menu callback for the settings form.
 */
function ajax_trigger_settings_form(&$form_state) {
  $form['ajax_trigger_global'] = array(
    '#type' => 'fieldset',
    '#title' => t('AJAX Trigger Field Settings'),
    //'#description' => t('AJAX Trigger Global Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['ajax_trigger_global']['ajax_trigger_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('JavaScript Update Timeout'),
    '#default_value' => variable_get('ajax_trigger_timeout', 500),
    '#field_suffix' => t('ms'),
  );


  $form['#validate'][] = 'ajax_trigger_form_validate';


  return system_settings_form($form);
}

/**
 *  Validation callback for the settings form.
 */
function ajax_trigger_form_validate($form, &$form_state){
  $timeout = $form['ajax_trigger_global']['ajax_trigger_timeout']['#value'];

  if ($timeout<=0){
    form_set_error('ajax_trigger_timeout', t("Timeout should be greater than 0."));
  }
}

